FROM golang:1.22-bookworm as build

RUN mkdir -p /go/src/gitlab.com/ahmadsherif/imap-mailbox-exporter
ADD . /go/src/gitlab.com/ahmadsherif/imap-mailbox-exporter

RUN cd /go/src/gitlab.com/ahmadsherif/imap-mailbox-exporter/ && \
    go build .

FROM debian:bookworm-slim

RUN apt update && apt install ca-certificates -y && apt clean

COPY --from=build /go/src/gitlab.com/ahmadsherif/imap-mailbox-exporter/imap-mailbox-exporter /usr/bin/

USER 1000

EXPOSE 9117
CMD ["imap-mailbox-exporter"]
